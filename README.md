# Using UltraViolet's API

This repository houses a helper Jupyter Notebook to walk colleagues through making requests to our InvenioRDM repository, UltraViolet.

## Install & dependencies

You need to have Python 3.10+ installed on your computer to run this repository.
  
First, step up a virtual environment using `virtualenv`. If you don't have this installed on your computer, do so: [https://virtualenv.pypa.io/en/latest/installation.html#via-pip](https://virtualenv.pypa.io/en/latest/installation.html#via-pip).

To run the notebooks in this repository, follow these steps via your terminal:

1. Create a virtual environment: `python3 virtualenv uv_api.venv`
2. Activate the virtual environment: `source uv_api.venv/bin/activate`
3. Change directory to your desktop so you know where you are on your computer: `cd ~/Desktop`
4. Clone this Git repository to your local computer: `git clone git@gitlab.com:VickyRampin/uv-api-exploration.git`
5. Enter this commend to change directory into the folder that was just created by Git: `cd uv-api-exploration`
6. Run this command to install all the dependencies in this repository: `pip install -r requirements.txt`
7. Next we'll run jupyter. A browser window will pop up with the notebooks listed. Use this command to start the jupyter notebook server: `jupyter notebooks`
8. Pick a notebook, and run it from top-to-bottom!